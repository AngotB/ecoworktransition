<!-- ##### Footer Area Start ##### -->
<footer class="footer-area">
    <!-- Main Footer Area -->
    <div class="main-footer-area section-padding-100-0">
        <div class="container">
            <div class="row">
                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-70 wow fadeInUp" data-wow-delay="100ms">
                        <div class="widget-title">
                            <a href="index.php"><img src="img/core-img/logo2.png" alt=""></a>
                        </div>
                        <div class="widget-content">
                            <p>Ecowork is a platform that facilitates and encourages users and companies to recycle their unused goods and trashs. Using a innovative reward system, sellers, buyers and donors will have a unique wallet that will keep in track every transactions they made and will be credited with cryptocurrency. The more you work the system the more the system works for you. </p>
                        </div>
                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-70 wow fadeInUp" data-wow-delay="300ms">
                        <div class="widget-title">
                            <h4>Key Words</h4>
                        </div>
                        <div class="widget-content">
                            <nav>
                                <ul>
                                    <li><a href="#">Environment</a></li>
                                    <li><a href="#">Protect</a></li>
                                    <li><a href="#">Recycle</a></li>
                                    <li><a href="#">Nature</a></li>
                                    <li><a href="#">Share</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-70 wow fadeInUp" data-wow-delay="500ms">
                        <div class="widget-title">
                            <h4>Usefull Links</h4>
                        </div>
                        <div class="widget-content">
                            <nav>
                                <ul>
                                    <li><a href="post.php">Buy</a></li>
                                    <li><a href="sell.php">Sell</a></li>
                                    <li><a href="donate.php">Donate</a></li>
                                    <li><a href="rewards.php">Rewards</a></li>
                                    <li><a href="contact.php">Contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-70 wow fadeInUp" data-wow-delay="700ms">
                        <div class="widget-title">
                            <h4>Our Team</h4>
                        </div>
                        <div class="widget-content">
                            <nav>
                                <ul>
                                    <li><a href="#">Jean-Marc / Jean-Baptiste</a></li>
                                    <li><a href="#">Jean-Luc / Emilie</a></li>
                                    <li><a href="#">Ahmed / Arthur</a></li>
                                    <li><a href="#">Baptiste Angot / Nicolas Dufresne</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Copywrite Area -->
    <div class="copywrite-content">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12 col-sm-5">
                    <!-- Copywrite Text -->
                    <p class="copywrite-text"><a href="#"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>
                <div class="col-12 col-sm-7">
                    <!-- Footer Nav -->
                    <div class="footer-nav">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="post.php">Shop</a></li>
                            <li><a href="donate.php">Donate</a></li>
                            <li><a href="rewards.php">Rewards</a></li>
                            <li><a href="contact.php">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- ##### Footer Area End ##### -->

<!-- ##### All Javascript Script ##### -->
<!-- jQuery-2.2.4 js -->
<script src="js/jquery/jquery-2.2.4.min.js"></script>
<!-- Popper js -->
<script src="js/bootstrap/popper.min.js"></script>
<!-- Bootstrap js -->
<script src="js/bootstrap/bootstrap.min.js"></script>
<!-- All Plugins js -->
<script src="js/plugins/plugins.js"></script>
<!-- Active js -->
<script src="js/active.js"></script>
</body>

</html>