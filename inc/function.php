<?php

function validationpseudo($error,$pseudo,$min,$max,$empty = true){
    if (!empty($pseudo)) {
        if(strlen($pseudo)<$min){
            $error['pseudo']= $min .' minimum characters';
        }
        elseif (strlen($pseudo)>$max) {
            $error['pseudo']= $max.' maximum characters';
        }
        else{
            //Verification si idverif existe déjà
            //Selection de $idverif de $table de la $bdd
            $resultat=verifuser($pseudo);
            if(!empty($resultat)){
                $error['pseudo']='mail already exist';
            }
        }
    }
    else{
        if($pseudo){
            $error['pseudo']='Please enter this field';
        }
    }
    return $error;
}

function validationpassword($error,$password1,$password2,$min,$max,$empty = true){
    global $pdo;
    if (!empty($password1)) {
        if($password1 != $password2){
            $error['password1'] = 'Error: Please enter this field';
        } elseif(strlen($password1)<$min){
            $error['password1']= $min .' minimum characters';
        } elseif (strlen($password1)>$max) {
            $error['password1']= $max.' maximum characters';
        }
    } else {
        if ($empty) {
            $error['password1'] = 'Erreur : password vide';
        }
    }
    return $error;
}

function generateRandomString($length) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function afficherErreur($error, $name)
{
    echo '<span class="error">';
    if (!empty($error[$name])) {
        echo $error[$name];
    }
    echo '</span>';
}

function isLogged()
{
    if (!empty($_SESSION['user']['id']) && !empty($_SESSION['user']['email'])) {
        if (is_numeric($_SESSION['user']['id'])) {
            return TRUE;
        }
    }
    return FALSE;
}


function isAdmin()
{
    if (!empty($_SESSION['user']['id']) && !empty($_SESSION['user']['email']) && $_SESSION['user']['ew_status'] == "admin" ) {
        if (is_numeric($_SESSION['user']['id'])) {
            return TRUE;
        }
    }
    return FALSE;
}

function validationText($error, $data, $min, $max, $key, $empty = true){
    if (!empty($data)){
        if(strlen($data) < $min ) {
            $error[$key] = 'trop court.';
        } elseif(strlen($key) > $max) {
            $error[$key] = 'trop long.';
        }
    } else {
        if ($empty) {
            $error[$key] = 'Veuillez renseignez ce champ';
        }
    }
    return $error;
}