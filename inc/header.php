<?php
    include 'pdo.php';
    include 'function.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>EcoWork</title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.ico">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">

</head>

<body>

<!-- ##### Header Area Start ##### -->
<header class="header-area wow fadeInDown" data-wow-delay="500ms">
    <!-- Top Header Area -->
    <div class="top-header-area">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12 d-flex align-items-center justify-content-between">
                    <!-- Logo Area -->
                    <div class="logo">
                        <a href=index.php><img src="img/core-img/logo.png" alt=""></a>
                    </div>

                    <!-- Search & Login Area -->
                    <div class="search-login-area d-flex align-items-center">
                        <!-- Top Search Area -->
                        <div class="top-search-area">
                            <form action="#" method="post">
                                <input type="search" name="top-search" id="topSearch" placeholder="Search">
                                <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                        <!-- Login Area -->
                        <div class="login-area">
                            <?php if(isLogged()){ ?>
                                <a href="disconnected.php"><span>Disconnected</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
                            <?php }else { ?>
                                <a href="login.php"><span>Login / Register</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Navbar Area -->
    <div class="egames-main-menu" id="sticker">
        <div class="classy-nav-container breakpoint-off">
            <div class="container">
                <!-- Menu -->
                <nav class="classy-navbar justify-content-between" id="egamesNav">

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Menu -->
                    <div class="classy-menu">

                        <!-- Close Button -->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>

                        <!-- Nav Start -->
                        <div class="classynav">
                            <ul>
                                <li><a href="index.php">Home</a></li>
                                <li><a href="">Shop</a>
                                    <ul class="dropdown">
                                        <li><a href="post.php">Buy</a></li>
                                        <li><a href="sell.php">Sell</a></li>
                                    </ul>
                                </li>
                                <li><a href="donate.php">Donate</a></li>
                                <li><a href="rewards.php">Rewards</a></li>
                                <li><a href="contact.php">Contact</a></li>
                                <?php if (isLogged()){ ?>
                                <li><a href="profiluser.php">Profil</a></li>
                                <?php } ?>
                            </ul>
                        </div>
                        <!-- Nav End -->
                    </div>

                    <!-- Top Social Info -->
                    <div class="top-social-info">
                        <a href="https://www.facebook.com/EcoWork-128264841457749/?modal=admin_todo_tour" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="https://twitter.com/work_eco?s=09" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>
<!-- ##### Header Area End ##### -->

<div id="wrapper">
