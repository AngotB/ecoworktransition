<?php

//Fonction pour select user bdd
function connect($login_mail,$password){
    global $pdo;

    $sql = "SELECT * FROM ew_users WHERE ew_mail = :login_mail";
    $query = $pdo -> prepare($sql);
    $query -> bindValue(':login_mail',$login_mail);
    $query -> execute();
    $user = $query -> fetch();
    return $user;
}

//Fonction pour vérifier si l'idverif est dans la BDD
function verifuser($email){
    global $pdo;
    $sql="SELECT ew_mail FROM ew_users WHERE ew_mail = :email";
    $query=$pdo->prepare($sql);
    $query->bindValue(':email',$email,PDO::PARAM_STR);
    $query->execute();
    $resultat = $query->fetch();
    return $resultat;
}

//Fonction pour se register
function register($email,$hash,$token){
    global $pdo;

    $sql = "INSERT INTO ew_users(ew_mail, ew_password, ew_created_at, ew_tokens) VALUES (:email, :password, NOW(), :token)";
    $query = $pdo -> prepare($sql);
    $query -> bindValue(':email', $email, PDO::PARAM_STR);
    $query -> bindValue(':password', $hash, PDO::PARAM_STR);
    $query -> bindValue(':token', $token, PDO::PARAM_STR);
    $query -> execute();
}

//Function for update profil
function updateprofil($firstname,$lastname,$phonenumb,$localisation,$wallet,$id){
    global $pdo;

    $sql= "UPDATE ew_users SET ew_first_name=:firstname, ew_last_name=:lastname, ew_phone_numb=:phonenumb,ew_localisation=:localisation, `ew_user_wallet-addr`=:wallet WHERE ew_id=:id";
    $query = $pdo->prepare($sql);
    $query->bindValue(':id',$id,PDO::PARAM_STR);
    $query->bindValue(':firstname',$firstname,PDO::PARAM_STR);
    $query->bindValue(':lastname',$lastname,PDO::PARAM_STR);
    $query->bindValue(':phonenumb',$phonenumb,PDO::PARAM_STR);
    $query->bindValue(':localisation',$localisation,PDO::PARAM_STR);
    $query->bindValue(':wallet',$wallet,PDO::PARAM_STR);
    $query -> execute();

}

function getinfouser($iduser){
    global $pdo;

    $sql="SELECT * FROM ew_users WHERE ew_id= :iduser";
    $query = $pdo->prepare($sql);
    $query->bindValue(':iduser',$iduser,PDO::PARAM_STR);
    $query->execute();
    $resultat = $query->fetch();
    return $resultat;
}

//Fonction pour effacer un utilisateur en fonction de son id
function deleteuser($id){
    global $pdo;

    $sql="DELETE FROM ew_users WHERE ew_id = :id";
    $query = $pdo ->prepare($sql);
    $query -> bindValue(':id',$id,PDO::PARAM_INT);
    $query -> execute();
}

//Fonction pour créer un item pour une donation
function insertitemdonation($item,$category,$userid,$info){
    global $pdo;

    $sql= "INSERT INTO ew_items(ew_id_user,ew_category,ew_created_at,ew_name,ew_info) VALUES (:userid,:category,NOW(),:itemname,:info)";
    $query = $pdo ->prepare($sql);
    $query -> bindValue(':userid',$userid,PDO::PARAM_INT);
    $query -> bindValue(':itemname',$item,PDO::PARAM_STR);
    $query -> bindValue(':category',$category,PDO::PARAM_STR);
    $query -> bindValue(':info',$info,PDO::PARAM_STR);
    $query -> execute();

}

function searchiditem($nameitem,$iduser){
    global $pdo;

    $sql= "SELECT ew_id_item FROM ew_items WHERE ew_name=:name AND ew_id_user=:userid";
    $query = $pdo ->prepare($sql);
    $query -> bindValue(':userid',$iduser,PDO::PARAM_INT);
    $query -> bindValue(':name',$nameitem,PDO::PARAM_STR);
    $query->execute();
    return $query->fetch();
}

function insertitemdonationtable($iditem,$nameassociation){
    global $pdo;

    $sql= "INSERT INTO ew_associations(ew_name,ew_created_at,ew_items_id) VALUES (:nameassociation,NOW(),:iditem)";
    $query = $pdo ->prepare($sql);
    $query -> bindValue(':nameassociation',$nameassociation,PDO::PARAM_STR);
    $query -> bindValue(':iditem',$iditem,PDO::PARAM_INT);
    $query->execute();


}

//Function register item
function register_item($category, $iduser,  $file, $info,$price){
    global $pdo;

    $sql = "INSERT INTO ew_items(ew_category, ew_id_user, ew_created_at, ew_info,ew_price) VALUES (:ew_category, :ew_id_user, NOW(), :info,:price)";
    $query = $pdo -> prepare($sql);
    $query -> bindValue(':ew_category', $category, PDO::PARAM_STR);
    $query -> bindValue(':price', $price, PDO::PARAM_INT);
    $query -> bindValue(':info', $info, PDO::PARAM_STR);
    $query -> bindValue(':ew_id_user', $iduser, PDO::PARAM_STR);
    $query -> execute();

    $sql = "SELECT * from ew_items where ew_id_item = (LAST_INSERT_ID())";

    $query = $pdo->prepare($sql);
    $query->execute();
    $resultat = $query->fetch();

    $idimage = ((int) (implode(", ", $resultat)));

    $extension = explode('.', $file['name']);
    $extension = end($extension);
    switch ($extension) {
        case "jpg":
        case "jpeg":
            $image = imagecreatefromjpeg($file["tmp_name"]);
            break;
        case "gif":
            $image = imagecreatefromgif($file["tmp_name"]);
            break;
        case "png":
            $image = imagecreatefrompng($file["tmp_name"]);
            break;
    }
    $dist = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR .
        "ecowork" . DIRECTORY_SEPARATOR .
        "img" . DIRECTORY_SEPARATOR .
        "bg-img" . DIRECTORY_SEPARATOR .
        $idimage . ".png";
    imagepng($image, $dist);
    imagedestroy($image);



}

function get_items($iduser){
    global $pdo;

    $sql="SELECT * FROM ew_items WHERE ew_id_user = :iduser AND ew_info = 'sell'";
    $query = $pdo->prepare($sql);
    $query->bindValue(':iduser',$iduser,PDO::PARAM_STR);
    $query->execute();
    return $query;
}

function delete_item($id){
    global $pdo;

    $sql="DELETE FROM ew_items WHERE ew_id_item = :id";
    $query = $pdo ->prepare($sql);
    $query -> bindValue(':id',$id,PDO::PARAM_INT);
    $query -> execute();
}

function update_item($category,  $idtem, $file){
    global $pdo;

    $sql= "UPDATE ew_items SET ew_category=:ew_category WHERE ew_id_item=:ew_id_item";
    $query = $pdo->prepare($sql);
    $query->bindValue(':ew_id_item',$idtem,PDO::PARAM_STR);
    $query->bindValue(':ew_category',$category,PDO::PARAM_STR);
    $query -> execute();

    $extension = explode('.', $file['name']);
    $extension = end($extension);
    switch ($extension) {
        case "jpg":
        case "jpeg":
            $image = imagecreatefromjpeg($file["tmp_name"]);
            break;
        case "gif":
            $image = imagecreatefromgif($file["tmp_name"]);
            break;
        case "png":
            $image = imagecreatefrompng($file["tmp_name"]);
            break;
    }
    $dist = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR .
        "ecowork" . DIRECTORY_SEPARATOR .
        "img" . DIRECTORY_SEPARATOR .
        "bg-img" . DIRECTORY_SEPARATOR .
        $idtem . ".png";
    imagepng($image, $dist);
    imagedestroy($image);


}
