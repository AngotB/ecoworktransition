<?php
include ('inc/header.php');
?>
<!DOCTYPE html>
<html lang="fr">




    <!-- ##### Hero Area Start ##### -->
    <div class="hero-area">
        <!-- Hero Post Slides -->
        <div class="hero-post-slides owl-carousel">

            <!-- Single Slide -->
            <div class="single-slide bg-img bg-overlay" style="background-image: url(img/bg-img/1.jpg);">
                <!-- Blog Content -->
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-12 col-lg-9">
                            <div class="blog-content" data-animation="fadeInUp" data-delay="100ms">
                                <h2 data-animation="fadeInUp" data-delay="400ms">Welcome to EcoWork</h2>
                                <p data-animation="fadeInUp" data-delay="700ms">Ecowork is a platform that facilitates and encourages users and company to recycle their unused goods and trashs. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Single Slide -->
            <div class="single-slide bg-img bg-overlay" style="background-image: url(img/bg-img/2.jpg);">
                <!-- Blog Content -->
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-12 col-lg-9">
                            <div class="blog-content" data-animation="fadeInUp" data-delay="100ms">
                                <h2 data-animation="fadeInUp" data-delay="400ms">You are the future</h2>
                                <p data-animation="fadeInUp" data-delay="700ms">Our company propose to sell/donate at other recycling company or association.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- ##### Hero Area End ##### -->

    <!-- ##### Games Area Start ##### -->
    <div class="games-area section-padding-100-0">
        <div class="container">
            <div class="row">
                <!-- Single Games Area -->
                <div class="col-12 col-md-4">
                    <div class="single-games-area text-center mb-100 wow fadeInUp" data-wow-delay="100ms">
                        <img src="img/bg-img/game4.jpg" alt="">
                        <a href="post.php" class="btn egames-btn mt-30">Buy</a>
                    </div>
                </div>

                <!-- Single Games Area -->
                <div class="col-12 col-md-4">
                    <div class="single-games-area text-center mb-100 wow fadeInUp" data-wow-delay="300ms">
                        <img src="img/bg-img/game5.jpg" alt="">
                        <a href="" class="btn egames-btn mt-30">Sell</a>
                    </div>
                </div>

                <!-- Single Games Area -->
                <div class="col-12 col-md-4">
                    <div class="single-games-area text-center mb-100 wow fadeInUp" data-wow-delay="500ms">
                        <img src="img/bg-img/game6.jpg" alt="">
                        <a href="" class="btn egames-btn mt-30">Donate</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Games Area End ##### -->





    <!-- ##### Articles Area Start ##### -->
    <section class="latest-articles-area section-padding-100-0 bg-img bg-pattern bg-fixed" style="background-image: url(img/bg-img/5.jpg);">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-8">
                    <div class="mb-100">
                        <!-- Title -->
                        <h2 class="section-title mb-70 wow fadeInUp" data-wow-delay="100ms">Users Advices</h2>

                        <!-- *** Single Articles Area *** -->
                        <div class="single-articles-area style-2 d-flex flex-wrap mb-30 wow fadeInUp" data-wow-delay="300ms">
                            <div class="article-thumbnail">
                                <img src="img/bg-img/6.jpg" alt="">
                            </div>
                            <div class="article-content">
                                <a href="single-post.html" class="post-title">Jessica Briet</a>
                                <div class="post-meta">
                                    <a href="#" class="post-date">October 6, 2018</a>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris velit arcu, scelerisque dignissim massa quis, mattis facilisis erat. Aliquam erat volutpat. Sed efficitur diam.</p>
                            </div>
                        </div>

                        <!-- *** Single Articles Area *** -->
                        <div class="single-articles-area style-2 d-flex flex-wrap mb-30 wow fadeInUp" data-wow-delay="500ms">
                            <div class="article-thumbnail">
                                <img src="img/bg-img/7.jpg" alt="">
                            </div>
                            <div class="article-content">
                                <a href="single-post.html" class="post-title">Joselito Furnborn</a>
                                <div class="post-meta">
                                    <a href="#" class="post-date">September 24, 2018</a>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris velit arcu, scelerisque dignissim massa quis, mattis facilisis erat. Aliquam erat volutpat. Sed efficitur diam.</p>
                            </div>
                        </div>

                        <!-- *** Single Articles Area *** -->
                        <div class="single-articles-area style-2 d-flex flex-wrap mb-30 wow fadeInUp" data-wow-delay="700ms">
                            <div class="article-thumbnail">
                                <img src="img/bg-img/8.jpg" alt="">
                            </div>
                            <div class="article-content">
                                <a href="single-post.html" class="post-title">Arnaud Gauthier-Fawas</a>
                                <div class="post-meta">
                                    <a href="#" class="post-date">July 12, 2018</a>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris velit arcu, scelerisque dignissim massa quis, mattis facilisis erat. Aliquam erat volutpat. Sed efficitur diam.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-4">
                    <!-- Title -->
                    <h2 class="section-title mb-70 wow fadeInUp" data-wow-delay="100ms">Picture of the week</h2>

                    <!-- Single Widget Area -->
                    <div class="single-widget-area add-widget wow fadeInUp" data-wow-delay="300ms">
                        <img src="img/bg-img/add.png"  alt="">
                        <!-- Side Img -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Articles Area End ##### -->

<?php include 'inc/footer.php';