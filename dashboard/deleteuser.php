<?php
include ("../inc/pdo.php");
include ("../inc/function.php");
include ("../inc/request.php");

if (isAdmin()) {

    if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
        $id = $_GET['id'];

        $sql = "DELETE FROM ew_users WHERE ew_id=:id";
        $query = $pdo->prepare($sql);
        $query->bindValue(':id', $id, PDO::PARAM_INT);
        $query->execute();
        header("Location: user.php");
    }
}