
<?php

include 'inc/pdo.php';
include 'inc/function.php';
include 'inc/request.php';

if(!isLogged()){

$error = array();

if (!empty($_POST['submitted'])){
    // Vérification pseudo
    $mail = trim(strip_tags($_POST['email']));
    $error = validationpseudo($error,$mail,3,50);

    // XSS Fail
    $password= trim(strip_tags($_POST['password']));
    $password2 = trim(strip_tags($_POST['password2']));

    $error = validationpassword($error,$password,$password2,3,50);

    if(count($error) ==0){
        // New user in database
        $hash = password_hash($password, PASSWORD_DEFAULT);
        $token = generateRandomString(50);

        register($mail,$hash,$token);
        header('Location: login.php');
    }
}

?>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<link rel="stylesheet" href="style.css">
<!------ Include the above in your HEAD tag ---------->


<html>
<head>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->
</head>
<body id="LoginForm">
<header class="header-area wow fadeInDown" data-wow-delay="500ms">
    <!-- Top Header Area -->
    <div class="top-header-area">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12 d-flex align-items-center justify-content-between">
                    <!-- Logo Area -->
                    <div class="logo">
                        <a href="index.html"><img src="img/core-img/logo.png" alt=""></a>
                    </div>

                    <!-- Search & Login Area -->
                    <div class="search-login-area d-flex align-items-center">
                        <!-- Top Search Area -->
                        <div class="top-search-area">
                            <form action="#" method="post">
                                <input type="search" name="top-search" id="topSearch" placeholder="Search">
                                <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                        <!-- Login Area -->
                        <div class="login-area">
                            <a href="#"><span>Login / Register</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Navbar Area -->
    <div class="egames-main-menu" id="sticker">
        <div class="classy-nav-container breakpoint-off">
            <div class="container">
                <!-- Menu -->
                <nav class="classy-navbar justify-content-between" id="egamesNav">

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Menu -->
                    <div class="classy-menu">

                        <!-- Close Button -->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>

                        <!-- Nav Start -->
                        <div class="classynav">
                            <ul>
                                <li><a href="index.html">Home</a></li>
                                <li><a href="game-review.html">Shop</a>
                                    <ul class="dropdown">
                                        <li><a href="post.html">Buy</a></li>
                                        <li><a href="single-post.html">Sell</a></li>
                                    </ul>
                                </li>
                                <li><a href="post.html">Donate</a></li>
                                <li><a href="single-game-review.html">Rewards</a></li>
                                <li><a href="contact.html">Contact</a></li>
                            </ul>
                        </div>
                        <!-- Nav End -->
                    </div>

                    <!-- Top Social Info -->
                    <div class="top-social-info">
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Dribbble"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Behance"><i class="fa fa-behance" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>

<div class="container">
    <div class="login-form">
        <div class="main-div">
            <div class="panel">
                <h2>Registration</h2>
                <p>Please enter your email, password and verified password</p>
            </div>


            <form id="Login" method="post">

                <div class="form-group">
                    <input type="text" class="form-control" id="inputEmail" name="email" placeholder="Email Address" required="" autofocus="" />
                    <?php afficherErreur($error, 'email');?>
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password" required=""/>
                    <?php afficherErreur($error, 'password');?>
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" id="inputPassword" name="password2" placeholder="Type your password again" required=""/>
                    <?php afficherErreur($error, 'password2');?>
                </div>

                <button type="submit" name="submitted" class="btn btn-primary" value="Login">Send</button>

            </form>


        </div>
        <p class="botto-text"> Designed by Sunil Rajput</p>
    </div></div>

<footer class="footer-area">
    <!-- Main Footer Area -->
    <div class="main-footer-area section-padding-100-0">
        <div class="container">
            <div class="row">
                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-70 wow fadeInUp" data-wow-delay="100ms">
                        <div class="widget-title">
                            <a href="index.html"><img src="img/core-img/logo2.png" alt=""></a>
                        </div>
                        <div class="widget-content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris velit arcu, scelerisque dignissim massa quis, mattis facilisis erat. Aliquam erat volutpat. Sed efficitur diam ut interdum ultricies.</p>
                        </div>
                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-70 wow fadeInUp" data-wow-delay="300ms">
                        <div class="widget-title">
                            <h4>Key Words</h4>
                        </div>
                        <div class="widget-content">
                            <nav>
                                <ul>
                                    <li><a href="#">Environment</a></li>
                                    <li><a href="#">Protect</a></li>
                                    <li><a href="#">Recycle</a></li>
                                    <li><a href="#">Nature</a></li>
                                    <li><a href="#">Share</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-70 wow fadeInUp" data-wow-delay="500ms">
                        <div class="widget-title">
                            <h4>Usefull Links</h4>
                        </div>
                        <div class="widget-content">
                            <nav>
                                <ul>
                                    <li><a href="#">Buy</a></li>
                                    <li><a href="#">Sell</a></li>
                                    <li><a href="#">Donate</a></li>
                                    <li><a href="#">Rewards</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-70 wow fadeInUp" data-wow-delay="700ms">
                        <div class="widget-title">
                            <h4>Our Team</h4>
                        </div>
                        <div class="widget-content">
                            <nav>
                                <ul>
                                    <li><a href="#">Jean-Marc / Jean-Baptiste</a></li>
                                    <li><a href="#">Jean-Luc / Emilie</a></li>
                                    <li><a href="#">Ahmed / Arthur</a></li>
                                    <li><a href="#">Baptiste Angot / Nicolas Dufresne</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Copywrite Area -->
    <div class="copywrite-content">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12 col-sm-5">
                    <!-- Copywrite Text -->
                    <p class="copywrite-text"><a href="#"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>
                <div class="col-12 col-sm-7">
                    <!-- Footer Nav -->
                    <div class="footer-nav">
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li><a href="game-review.html">Shop</a></li>
                            <li><a href="post.html">Donate</a></li>
                            <li><a href="single-game-review.html">Rewards</a></li>
                            <li><a href="contact.html">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>


</body>
</html>
<?php }else { header('location: index.php'); }