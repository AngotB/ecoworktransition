<?php
include('inc/pdo.php');
include('inc/function.php');
include('inc/request.php');

if (!empty($_GET['id']) && is_numeric($_GET['id'])){
    $id = $_GET['id'];
    delete_item($id);
    header("Location: sell.php");
}