<?php

include 'inc/pdo.php';
include 'inc/function.php';
include 'inc/request.php';

if (!empty($_POST['submitted'])){


    $category = trim(strip_tags($_POST['category']));
    $price = trim(strip_tags($_POST['price']));

    $info = "sell";


    if($_FILES["file"]["name"] !== "") register_item($category, $_SESSION['user']['id'], $_FILES["file"],$info,$price);



    header('Location: sell.php');

}

if (!empty($_POST['submitted_update'])){

    $idtem = trim(strip_tags($_POST['idtem']));

    $category = trim(strip_tags($_POST['category']));

    if($_FILES["file"]["name"] !== "") update_item($category, $idtem, $_FILES["file"]);

    header('Location: sell.php');

}

?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <!-- Title -->
        <title>EcoWork</title>

        <!-- Favicon -->
        <link rel="icon" href="img/core-img/favicon.ico">

        <!-- Stylesheet -->
        <link rel="stylesheet" href="style.css">

    </head>

<body>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area wow fadeInDown" data-wow-delay="500ms">
        <!-- Top Header Area -->
        <div class="top-header-area">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12 d-flex align-items-center justify-content-between">
                        <!-- Logo Area -->
                        <div class="logo">
                            <a href=index.php><img src="img/core-img/logo.png" alt=""></a>
                        </div>

                        <!-- Search & Login Area -->
                        <div class="search-login-area d-flex align-items-center">
                            <!-- Top Search Area -->
                            <div class="top-search-area">
                                <form action="#" method="post">
                                    <input type="search" name="top-search" id="topSearch" placeholder="Search">
                                    <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                            <!-- Login Area -->
                            <div class="login-area">
                                <?php if(isLogged()){ ?>
                                    <a href="disconnected.php"><span>Disconnected</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
                                <?php }else { ?>
                                    <a href="login.php"><span>Login / Register</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Navbar Area -->
        <div class="egames-main-menu" id="sticker">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="egamesNav">

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li><a href="index.php">Home</a></li>
                                    <li><a>Shop</a>
                                        <ul class="dropdown">
                                            <li><a href="post.php">Buy</a></li>
                                            <li><a href="sell.php">Sell</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="donate.php">Donate</a></li>
                                    <li><a href="rewards.php">Rewards</a></li>
                                    <li><a href="contact.php">Contact</a></li>
                                    <?php if (isLogged()){ ?>
                                        <li><a href="profiluser.php">Profil</a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <!-- Nav End -->
                        </div>

                        <!-- Top Social Info -->
                        <div class="top-social-info">
                            <a href="https://www.facebook.com/EcoWork-128264841457749/?modal=admin_todo_tour" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="https://twitter.com/work_eco?s=09" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->

    <!-- ##### Breadcrumb Area Start ##### -->
    <section class="breadcrumb-area bg-img bg-overlay" style="background-image: url(img/bg-img/57.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <!-- Breadcrumb Text -->
                <div class="col-12">
                    <div class="breadcrumb-text">
                        <h2>Sell</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Breadcrumb Area End ##### -->

<div id="wrapper">

<div id="wrapper">

<body>

<div class="container-fluid">

    <div class="container">

        <h1 class="my-4 text-center text-lg-left">My Items</h1>

        <div>

        </div>

        <div class="row text-center text-lg-left">

            <?php

            $items = get_items($_SESSION['user']['id']);

            foreach ($items as $row) {

                print '
    <div class="col-lg-3 col-md-4 col-xs-6">
              <a data-toggle="modal" href="#modal_update_'.
                    $row["ew_id_item"].'" class="d-block mb-4 h-100">
                <img class="img-fluid img-thumbnail" src="/ecowork/img/bg-img/'.
                    $row["ew_id_item"].'.png">
              </a>
              
            </div>
                                               
                        <div id="modal_update_'.$row["ew_id_item"].'" class="modal fade" role="dialog">
                              <div class="modal-dialog">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <h5 class="modal-title" id="exampleModalLongTitle">Edit Item</h5>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                          </button>
                                        </div>
                                      <div class="modal-body">
                                          <form method="post" enctype="multipart/form-data">
                                             <input type="hidden" name="idtem" value="'.$row["ew_id_item"].'">
                                              <div class="box-body">
                                                   <div class="form-group">
        <label for="exampleFormControlSelect1">Category</label>
        <select class="form-control" name="category">
        <option selected="'.$row["ew_category"].'">'.$row["ew_category"].'</option>
          <option value="VEHICLES">VEHICLES</option>
          <option value="MULTIMEDIA">MULTIMEDIA</option>
          <option value="PROFESSIONAL EQUIPMENT">PROFESSIONAL EQUIPMENT</option>
          <option value="HOUSE">HOUSE</option>
          <option value="FASHION">FASHION</option>
          <option value="SERVICES">SERVICES</option>
          <option value="HOBBIES">HOBBIES</option>
        </select>
      </div>
                                                  <div class="form-group">
                                        <label for="file">Change image</label>
                                                      <div class="custom-file">
          <input type="text" class="custom-file-input" id="prince" name="prince" value="">
        </div>
        
        <div class="form-group">
        
        
        
                                         </div>
                                         <div class="form-group">
                                                 <div class="col-lg-3 col-md-4 col-xs-6">

                    <img class="img-fluid img-thumbnail" src="/ecowork/img/bg-img/'.
                    $row["ew_id_item"].'.png">

                </div>
                                            </div>

                                              </div>
                                              <div class="box-footer">
                                                  

                                                   <div class="box-footer">
                                                <input class = "btn btn-primary" type="submit" name="submitted_update" value="Update">
                                                <a class = "btn btn-danger" href="delete_product.php?id='.$row["ew_id_item"].'">Delete</a>
                                            </div>
                                             
                                              </div>
                                          </form>
                                      </div>
                                  </div>
                              </div>
                          </div>';

            }

            ?>

        </div>

        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#item_create">
            New item
        </button>

    </div>

    <div class="modal fade" id="item_create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">New item</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" enctype="multipart/form-data">

                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Category</label>
                                <select class="form-control" name="category">
                                    <option value="VEHICLES">VEHICLES</option>
                                    <option value="MULTIMEDIA">MULTIMEDIA</option>
                                    <option value="PROFESSIONAL EQUIPMENT">PROFESSIONAL EQUIPMENT</option>
                                    <option value="HOUSE">HOUSE</option>
                                    <option value="FASHION">FASHION</option>
                                    <option value="SERVICES">SERVICES</option>
                                    <option value="HOBBIES">HOBBIES</option>
                                </select>
                            </div>
                            <div class="form-group">

                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="file" name="file">
                                    <label class="custom-file-label" for="customFileLang">Photo</label>
                                </div>
                            </div>

                            <input type="text" id="price" name="prince" placeholder="price">

                        </div>

                        <div class="box-footer">
                            <input type="submit" name="submitted" value="Save">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php

    foreach ($items as $row) {




    }

    ?>



</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</body>
<?php include('inc/footer.php');?>