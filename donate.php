<?php
include 'inc/pdo.php';
include 'inc/function.php';
include 'inc/request.php';

if (isLogged()){

$error = array();

if (!empty($_POST['submitted'])) {

    // XSS fail
    $nameitem = trim(strip_tags($_POST['item']));
    $category = trim(strip_tags($_POST['category']));
    $association = trim(strip_tags($_POST['association']));
    $info = 'donation';

    $error=validationText($error,$nameitem,3,100,'item',false);

    if(count($error) === 0){
        insertitemdonation($nameitem,$category,$_SESSION['user']['id'],$info);
        $old = searchiditem($nameitem,$_SESSION['user']['id']);
        $iditem = $old['ew_id_item'];
        insertitemdonationtable($iditem,$association);
        header('location: index.php');
    }



}
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <!-- Title -->
        <title>EcoWork</title>

        <!-- Favicon -->
        <link rel="icon" href="img/core-img/favicon.ico">

        <!-- Stylesheet -->
        <link rel="stylesheet" href="style.css">

    </head>

    <body>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area wow fadeInDown" data-wow-delay="500ms">
        <!-- Top Header Area -->
        <div class="top-header-area">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12 d-flex align-items-center justify-content-between">
                        <!-- Logo Area -->
                        <div class="logo">
                            <a href=index.php><img src="img/core-img/logo.png" alt=""></a>
                        </div>

                        <!-- Search & Login Area -->
                        <div class="search-login-area d-flex align-items-center">
                            <!-- Top Search Area -->
                            <div class="top-search-area">
                                <form action="#" method="post">
                                    <input type="search" name="top-search" id="topSearch" placeholder="Search">
                                    <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                            <!-- Login Area -->
                            <div class="login-area">
                                <a href="login.php"><span>Login / Register</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Navbar Area -->
        <div class="egames-main-menu" id="sticker">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="egamesNav">

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li><a href="index.php">Home</a></li>
                                    <li><a>Shop</a>
                                        <ul class="dropdown">
                                            <li><a href="post.php">Buy</a></li>
                                            <li><a href="sell.php">Sell</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="donate.php">Donate</a></li>
                                    <li><a href="single-game-review.html">Rewards</a></li>
                                    <li><a href="contact.php">Contact</a></li>
                                    <li><a href="profiluser.php">Profil</a></li>
                                </ul>
                            </div>
                            <!-- Nav End -->
                        </div>

                        <!-- Top Social Info -->
                        <div class="top-social-info">
                            <a href="https://www.facebook.com/EcoWork-128264841457749/?modal=admin_todo_tour" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="https://twitter.com/work_eco?s=09" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->

    <!-- ##### Breadcrumb Area Start ##### -->
    <section class="breadcrumb-area bg-img bg-overlay" style="background-image: url(img/bg-img/56.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <!-- Breadcrumb Text -->
                <div class="col-12">
                    <div class="breadcrumb-text">
                        <h2>Donate</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Breadcrumb Area End ##### -->

    <div id="wrapper">


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-sm-6">

                <div class="card hovercard">
                    <div class="cardheader2">

                    </div>
                    <div class="info">
                        <div class="title">
                            <div class="wrapper">
                                <form class="" method="post">
                                <div class="form-group">
                                    <label for="item">Item</label>
                                    <input type="text" class="form-control" id="item" name="item" placeholder="The item we want to donate">
                                </div>
                                <div class="form-group">
                                    <label for="category">Category of item: </label>
                                    <select class="form-control" id="category" name="category" >
                                        <option>Vehicles</option>
                                        <option>House</option>
                                        <option>Multimedia</option>
                                        <option>profesional equipment</option>
                                        <option>Fashion</option>
                                        <option>Services</option>
                                        <option>Leisure</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="association">Association who want to give: </label>
                                    <select class="form-control" id="association" name="association">
                                        <option>Recygo</option>
                                        <option>CPA Recyclage</option>
                                        <option>Veolia</option>
                                    </select>
                                </div>
                                <input type="submit" class="btn btn-primary" name="submitted" value="Submit">
                            </div>
                            </div>
                    </div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
<?php }else{die('403');}

include 'inc/footer.php';